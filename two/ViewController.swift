//
//  ViewController.swift
//  two
//
//  Created by Krzysztof Marczewski on 12.01.2018.
//  Copyright © 2018 Veriqus. All rights reserved.
//

import UIKit
import CoreLocation
import Foundation
import MapKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate {

    let GIOS_STACJE_URL = "http://api.gios.gov.pl/pjp-api/rest/station/findAll"
    let GIOS_STANOWISKA_URL = "http://api.gios.gov.pl/pjp-api/rest/station/sensors/"
    let GIOS_DANE_URL = "http://api.gios.gov.pl/pjp-api/rest/data/getData/"
    let GIOS_QUALITY_URL = "http://api.gios.gov.pl/pjp-api/rest/aqindex/getIndex/"
    
    let locationManager = CLLocationManager();
    let geoCoder = CLGeocoder()
    
    var stationID = 0
    var city = ""
    
    
    var so2 = 0;
    var no2 = 0;
    var pm10 = 0;
    var pm25 = 0;
    var co = 0;
    var c6h6 = 0;
    var o3 = 0;
    
    var airQuality = ""
    
    var data_value : Double = 0
    var data_key = ""
    var data_date = ""

    
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var SO2label: UILabel!
    @IBOutlet weak var NO2label: UILabel!
    @IBOutlet weak var PM10label: UILabel!
    @IBOutlet weak var PM25label: UILabel!
    @IBOutlet weak var COlabel: UILabel!
    @IBOutlet weak var C6H6label: UILabel!
    @IBOutlet weak var O3label: UILabel!
    @IBOutlet weak var generalQuailityLabel: UILabel!
    
    @IBOutlet weak var SO2button: UIButton!
    @IBOutlet weak var NO2button: UIButton!
    @IBOutlet weak var PM10button: UIButton!
    @IBOutlet weak var PM25button: UIButton!
    @IBOutlet weak var CObutton: UIButton!
    @IBOutlet weak var C6H6button: UIButton!
    @IBOutlet weak var O3button: UIButton!
    
    @IBOutlet weak var cityField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        cityField.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count - 1]
        if location.horizontalAccuracy > 0 {
            locationManager.stopUpdatingLocation()
            locationManager.delegate = nil
            
            print(location.coordinate.longitude)
        }
        
        //changing coordinates to city name:
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error) in
            if (error != nil) {
                print("Error in reverseGeocode")
            }
            
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count > 0 {
                let placemark = placemarks![0]
                self.city = placemark.locality!
                self.cityNameLabel.text = self.city
                self.getStationID(cityPassed: self.city)
            }
        })
    }
    
    func textFieldShouldReturn(_ cityField: UITextField) -> Bool {
        // Hide the keyboard.
        cityField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ cityField: UITextField) {
        city = cityField.text!
        swapTextFields()
        clearData()
        getStationID(cityPassed: city)
        cityNameLabel.text = city
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
        cityNameLabel.text = "Location Unavailabele"
    }
    
    func getColorRepresentationData(station: Int) {
        let customURL = GIOS_QUALITY_URL + String(station)
        print(customURL)
        
        Alamofire.request(customURL, method: .get).responseJSON {
            response in
            if response.result.isSuccess {
                print("Success, colors loaded")
                let colorIdJSON : JSON = JSON(response.result.value!)
                if colorIdJSON["stIndexLevel"] != JSON.null {
                    self.generalQuailityLabel.text = colorIdJSON["stIndexLevel"]["indexLevelName"].string!.uppercased()
                    switch colorIdJSON["stIndexLevel"]["id"].int! {
                        case 0: self.generalQuailityLabel.backgroundColor = self.hexStringToUIColor(hex: "#56B230")
                        case 1: self.generalQuailityLabel.backgroundColor = self.hexStringToUIColor(hex: "#B2DD3F")
                        case 2: self.generalQuailityLabel.backgroundColor = self.hexStringToUIColor(hex: "#FFD83F")
                        case 3: self.generalQuailityLabel.backgroundColor = self.hexStringToUIColor(hex: "#EA7E22")
                        case 4: self.generalQuailityLabel.backgroundColor = self.hexStringToUIColor(hex: "#EA0009")
                        case 5: self.generalQuailityLabel.backgroundColor = self.hexStringToUIColor(hex: "#9C0004")
                        default: ()
                    }
                }
                if colorIdJSON["so2IndexLevel"] != JSON.null {
                    switch colorIdJSON["so2IndexLevel"]["id"].int! {
                        case 0: self.SO2button.setBackgroundImage(UIImage(named: "circle_0"), for: UIControlState.normal)
                        case 1: self.SO2button.setBackgroundImage(UIImage(named: "circle_1"), for: UIControlState.normal)
                        case 2: self.SO2button.setBackgroundImage(UIImage(named: "circle_2"), for: UIControlState.normal)
                        case 3: self.SO2button.setBackgroundImage(UIImage(named: "circle_3"), for: UIControlState.normal)
                        case 4: self.SO2button.setBackgroundImage(UIImage(named: "circle_4"), for: UIControlState.normal)
                        case 5: self.SO2button.setBackgroundImage(UIImage(named: "circle_5"), for: UIControlState.normal)
                        default: ()
                    }
                }
                if colorIdJSON["no2IndexLevel"] != JSON.null {
                    switch colorIdJSON["no2IndexLevel"]["id"].int! {
                    case 0: self.NO2button.setBackgroundImage(UIImage(named: "circle_0"), for: UIControlState.normal)
                    case 1: self.NO2button.setBackgroundImage(UIImage(named: "circle_1"), for: UIControlState.normal)
                    case 2: self.NO2button.setBackgroundImage(UIImage(named: "circle_2"), for: UIControlState.normal)
                    case 3: self.NO2button.setBackgroundImage(UIImage(named: "circle_3"), for: UIControlState.normal)
                    case 4: self.NO2button.setBackgroundImage(UIImage(named: "circle_4"), for: UIControlState.normal)
                    case 5: self.NO2button.setBackgroundImage(UIImage(named: "circle_5"), for: UIControlState.normal)
                    default: ()
                    }
                }
                if colorIdJSON["pm10IndexLevel"] != JSON.null {
                    switch colorIdJSON["pm10IndexLevel"]["id"].int! {
                    case 0: self.PM10button.setBackgroundImage(UIImage(named: "circle_0"), for: UIControlState.normal)
                    case 1: self.PM10button.setBackgroundImage(UIImage(named: "circle_1"), for: UIControlState.normal)
                    case 2: self.PM10button.setBackgroundImage(UIImage(named: "circle_2"), for: UIControlState.normal)
                    case 3: self.PM10button.setBackgroundImage(UIImage(named: "circle_3"), for: UIControlState.normal)
                    case 4: self.PM10button.setBackgroundImage(UIImage(named: "circle_4"), for: UIControlState.normal)
                    case 5: self.PM10button.setBackgroundImage(UIImage(named: "circle_5"), for: UIControlState.normal)
                    default: ()
                    }
                }
                if colorIdJSON["pm25IndexLevel"] != JSON.null {
                    switch colorIdJSON["pm25IndexLevel"]["id"].int! {
                    case 0: self.PM25button.setBackgroundImage(UIImage(named: "circle_0"), for: UIControlState.normal)
                    case 1: self.PM25button.setBackgroundImage(UIImage(named: "circle_1"), for: UIControlState.normal)
                    case 2: self.PM25button.setBackgroundImage(UIImage(named: "circle_2"), for: UIControlState.normal)
                    case 3: self.PM25button.setBackgroundImage(UIImage(named: "circle_3"), for: UIControlState.normal)
                    case 4: self.PM25button.setBackgroundImage(UIImage(named: "circle_4"), for: UIControlState.normal)
                    case 5: self.PM25button.setBackgroundImage(UIImage(named: "circle_5"), for: UIControlState.normal)
                    default: ()
                    }
                }
                if colorIdJSON["coIndexLevel"] != JSON.null {
                    switch colorIdJSON["coIndexLevel"]["id"].int! {
                    case 0: self.CObutton.setBackgroundImage(UIImage(named: "circle_0"), for: UIControlState.normal)
                    case 1: self.CObutton.setBackgroundImage(UIImage(named: "circle_1"), for: UIControlState.normal)
                    case 2: self.CObutton.setBackgroundImage(UIImage(named: "circle_2"), for: UIControlState.normal)
                    case 3: self.CObutton.setBackgroundImage(UIImage(named: "circle_3"), for: UIControlState.normal)
                    case 4: self.CObutton.setBackgroundImage(UIImage(named: "circle_4"), for: UIControlState.normal)
                    case 5: self.CObutton.setBackgroundImage(UIImage(named: "circle_5"), for: UIControlState.normal)
                    default: ()
                    }
                }
                if colorIdJSON["c6h6IndexLevel"] != JSON.null {
                    switch colorIdJSON["c6h6IndexLevel"]["id"].int! {
                    case 0: self.C6H6button.setBackgroundImage(UIImage(named: "circle_0"), for: UIControlState.normal)
                    case 1: self.C6H6button.setBackgroundImage(UIImage(named: "circle_1"), for: UIControlState.normal)
                    case 2: self.C6H6button.setBackgroundImage(UIImage(named: "circle_2"), for: UIControlState.normal)
                    case 3: self.C6H6button.setBackgroundImage(UIImage(named: "circle_3"), for: UIControlState.normal)
                    case 4: self.C6H6button.setBackgroundImage(UIImage(named: "circle_4"), for: UIControlState.normal)
                    case 5: self.C6H6button.setBackgroundImage(UIImage(named: "circle_5"), for: UIControlState.normal)
                    default: ()
                    }
                }
                if colorIdJSON["o3IndexLevel"] != JSON.null {
                    switch colorIdJSON["o3IndexLevel"]["id"].int! {
                    case 0: self.O3button.setBackgroundImage(UIImage(named: "circle_0"), for: UIControlState.normal)
                    case 1: self.O3button.setBackgroundImage(UIImage(named: "circle_1"), for: UIControlState.normal)
                    case 2: self.O3button.setBackgroundImage(UIImage(named: "circle_2"), for: UIControlState.normal)
                    case 3: self.O3button.setBackgroundImage(UIImage(named: "circle_3"), for: UIControlState.normal)
                    case 4: self.O3button.setBackgroundImage(UIImage(named: "circle_4"), for: UIControlState.normal)
                    case 5: self.O3button.setBackgroundImage(UIImage(named: "circle_5"), for: UIControlState.normal)
                    default: ()
                    }
                }
            }
            else {
                print("Error \(response.result.error)")
            }
        }
    }
    
    
    func getStandID(station: Int) {
        var standID = 0
        
        let customURL = GIOS_STANOWISKA_URL + String(station)
        print(customURL)
        
        Alamofire.request(customURL, method: .get).responseJSON {
            response in
            if response.result.isSuccess {
                print("Success, standID loaded")

                let standIdJSON : JSON = JSON(response.result.value!)
                for index in 0...standIdJSON.count-1 {
                        standID = standIdJSON[index]["id"].int!
                        print(standID)
                        self.getAirQuailtyData(standID: standID)
                }
            }
            else {
                print("Error \(response.result.error)")
            }
        }
    }
    
    
    func getAirQuailtyData(standID: Int) {
        
        let customURL = GIOS_DANE_URL + String(standID)
        print(customURL)
        var keyValueData = ""
        
        Alamofire.request(customURL, method: .get).responseJSON {
            response in
            if response.result.isSuccess {
                print("Success, data loaded")
        
                let dataIdJSON : JSON = JSON(response.result.value!)
                for index in 0...dataIdJSON["values"].count-1 {
                    if dataIdJSON["values"][index]["value"].double != nil {
                        self.data_value = dataIdJSON["values"][index]["value"].double!
                        //print(self.data_value)
                        self.data_key = dataIdJSON["key"].string!
                        //print(self.data_key)
                        self.data_date = dataIdJSON["values"][index]["date"].string!
                        //print(self.data_date)
                        keyValueData = String(self.data_value) + "  ::  " + self.data_date
                        switch self.data_key {
                            case "PM10": self.PM10label.text = keyValueData
                            case "PM2.5": self.PM25label.text = keyValueData
                            case "C6H6": self.C6H6label.text = keyValueData
                            case "CO": self.COlabel.text = keyValueData
                            case "NO2": self.NO2label.text = keyValueData
                            case "O3": self.O3label.text = keyValueData
                            case "SO2": self.SO2label.text = keyValueData
                            default: print(keyValueData)
                        }
                        break
                    }
                }
            }
            else {
                print("Error \(response.result.error)")
            }
        }
    }
    
    func getStationID(cityPassed: String){
        var stationName = ""
        
        Alamofire.request(GIOS_STACJE_URL, method: .get).responseJSON {
            response in
            if response.result.isSuccess {
                print("Success, station ID loaded")
                
                let stationsIdJSON : JSON = JSON(response.result.value!)
                for index in 0...stationsIdJSON.count-1 {
                    if stationsIdJSON[index]["city"]["name"].string != nil {
                            stationName = stationsIdJSON[index]["city"]["name"].string!
                            if (cityPassed == stationName){
                            self.stationID = stationsIdJSON[index]["id"].int!
                                print(self.stationID)
                            print(stationName)
                            self.getStandID(station: self.stationID)
                            self.getColorRepresentationData(station: self.stationID)
                            break
                        }
                    }
                }
            }
            else {
                print("Error \(response.result.error)")
            }
        }
    }
    
    //from /https://stackoverflow.com/questions/24263007/how-to-use-hex-colour-values
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "changeCityNameSegue" {
            
        }
    }
    
    //MARK: Actions
    
    @IBAction func reloadButton(_ sender: UIButton) {
        self.getStationID(cityPassed: self.city)
    }
    
    @IBAction func changeCityButton(_ sender: UIButton) {
        swapTextFields()
        cityField.resignFirstResponder()
    }
    
    func clearData() {
        self.SO2label.text = "nodata"
        self.NO2label.text = "nodata"
        self.PM10label.text = "nodata"
        self.PM25label.text = "nodata"
        self.COlabel.text = "nodata"
        self.C6H6label.text = "nodata"
        self.O3label.text = "nodata"
        
        self.SO2button.setBackgroundImage(UIImage(named: "grayCircle"), for: UIControlState.normal)
        self.NO2button.setBackgroundImage(UIImage(named: "grayCircle"), for: UIControlState.normal)
        self.PM10button.setBackgroundImage(UIImage(named: "grayCircle"), for: UIControlState.normal)
        self.PM25button.setBackgroundImage(UIImage(named: "grayCircle"), for: UIControlState.normal)
        self.CObutton.setBackgroundImage(UIImage(named: "grayCircle"), for: UIControlState.normal)
        self.O3button.setBackgroundImage(UIImage(named: "grayCircle"), for: UIControlState.normal)
        self.C6H6button.setBackgroundImage(UIImage(named: "grayCircle"), for: UIControlState.normal)
        
        self.generalQuailityLabel.text = "NO DATA"
        self.generalQuailityLabel.backgroundColor = self.hexStringToUIColor(hex: "#CCCCCC")
        
    }
    
    func swapTextFields() {
        if cityField.isHidden {
            cityField.isHidden = false
            cityNameLabel.isHidden = true
        }
        else {
            cityField.isHidden = true
            cityNameLabel.isHidden = false
        }
    }
    
    
}

